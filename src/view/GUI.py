import sys
import numpy
import itertools
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QSizePolicy, QPushButton, QLabel, QScrollArea, QFrame, QLineEdit, QFileDialog, QSlider, QCheckBox
from PyQt5.QtGui import QPainter, QPolygonF, QColor, QTransform, QPixmap
from PyQt5.QtCore import QPointF, Qt


class GUI(QApplication):
    def __init__(self, title, land_shapes, hdf_manager):
        super(QApplication, self).__init__(sys.argv)
        self.main_window = GUI.MainWindow(title, hdf_manager)

        self.main_window.map_hdf_list_layout.set_land_shapes(land_shapes)

    def __enter__(self):
        """
        GUI Context manager begin
        :return: self
        """
        self.main_window.show()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        GUI Context manager end
        Upon destroying self exit gracefully
        """
        sys.exit(self.exec_())

    class MainWindow(QWidget):
        def __init__(self, title, hdf_manager):
            # Window dimensions / title
            super(QWidget, self).__init__()
            self.title = title
            self.left = 30
            self.top = 30
            self.width = 1280
            self.height = 720
            self.setWindowTitle(self.title)
            self.setGeometry(self.left, self.top, self.width, self.height)

            self.main_layout = QVBoxLayout()
            self.map_hdf_list_layout = self.MapHDFListLayout(hdf_manager)

            # Download view
            self.download_window = self.DownloadWindow(hdf_manager, self.map_hdf_list_layout.hdf_button_list_view)
            self.download_view_button = QPushButton('Open Download Window')
            self.download_view_button.clicked.connect(self.download_window.show)

            # Add children to main layout
            self.main_layout.addLayout(self.map_hdf_list_layout)
            self.main_layout.addWidget(self.download_view_button)
            self.setLayout(self.main_layout)

            self.show()

        class MapHDFListLayout(QHBoxLayout):
            def __init__(self, hdf_manager):
                super(QHBoxLayout, self).__init__()

                # Multiplier for map scale
                self.map_scale = 1

                self.map_view = self.MapView()

                self.clear_overlay_points_button = QPushButton('Clear Overlay Points')
                self.clear_overlay_points_button.clicked.connect(self.map_view.clear_overlay_points)

                def save_map_image():
                    self.map_view.save_map_image(self.save_dialog.getSaveFileName()[0])

                self.save_map_button = QPushButton('Save Map...')
                self.save_map_button.clicked.connect(save_map_image)

                self.save_dialog = QFileDialog()

                def change_map_scale_function(text_input):
                    def change_map_scale():
                        try:
                            self.map_scale = float(text_input.text())
                            self.map_view.set_scale(self.map_scale)
                        except ValueError as e:
                            print 'Input Scale is not a number: ', e.message

                    return change_map_scale

                self.map_scale_input = QLineEdit('Input Map Scale')
                self.map_scale_input.editingFinished.connect(change_map_scale_function(self.map_scale_input))

                self.map_controls_layout = QHBoxLayout()
                self.map_controls_layout.addWidget(self.map_scale_input)
                self.map_controls_layout.addWidget(self.clear_overlay_points_button)
                self.map_controls_layout.addWidget(self.save_map_button)

                def overlay_checked_boxes():
                    for parser in self.map_view.selected_hdfs.values():
                        self.map_view.set_overlay_points(parser)

                def clear_checked_boxes():
                    for box in self.map_view.selected_hdfs.keys():
                        box.setCheckState(Qt.Unchecked)

                self.hdf_button_list_view = self.HDFButtonListView(hdf_manager, self.map_view)
                self.check_box_controls_layout = QHBoxLayout()
                self.add_all_checked_boxes_button = QPushButton('Add all checked files...')
                self.deselect_check_boxes_button = QPushButton('Clear selection')
                self.add_all_checked_boxes_button.clicked.connect(overlay_checked_boxes)
                self.deselect_check_boxes_button.clicked.connect(clear_checked_boxes)
                self.hdf_button_list_layout = QVBoxLayout()
                self.check_box_controls_layout.addWidget(self.add_all_checked_boxes_button)
                self.check_box_controls_layout.addWidget(self.deselect_check_boxes_button)
                self.hdf_button_list_layout.addWidget(self.hdf_button_list_view)
                self.hdf_button_list_layout.addLayout(self.check_box_controls_layout)

                self.vapor_threshold_slider = QSlider()
                self.vapor_threshold_slider.setOrientation(Qt.Horizontal)
                self.vapor_threshold_slider.setMaximum(99)
                self.vapor_threshold_label = QLabel(str(self.vapor_threshold_slider.value()))

                def update_slider_label():
                    self.vapor_threshold_label.setText(str(self.vapor_threshold_slider.value()))

                def set_min_vapor_threshold():
                    self.map_view.set_min_vapor_threshold(self.vapor_threshold_slider.value())

                self.vapor_threshold_slider.valueChanged.connect(update_slider_label)
                self.vapor_threshold_slider.sliderReleased.connect(set_min_vapor_threshold)

                self.vapor_threshold_layout = QHBoxLayout()
                self.vapor_threshold_layout.addWidget(self.vapor_threshold_slider)
                self.vapor_threshold_layout.addWidget(self.vapor_threshold_label)

                # Map and Scale input textbox
                self.map_layout = QVBoxLayout()
                self.map_layout.addWidget(self.map_view)
                self.map_layout.addLayout(self.map_controls_layout)
                self.map_layout.addLayout(self.vapor_threshold_layout)

                self.addLayout(self.map_layout)
                self.addLayout(self.hdf_button_list_layout)

            def set_land_shapes(self, land_shapes):
                self.map_view.set_land_shapes(land_shapes)

            def set_overlay_points(self, hdf_parser_dataset):
                self.map_view.set_overlay_points(hdf_parser_dataset)

            class MapView(QScrollArea):
                QT_COLOR_WHITE = QColor.fromRgb(255, 255, 255)

                def __init__(self):
                    super(QWidget, self).__init__()

                    self.min_vapor_threshold = 0

                    # Allows widgets to resize IMPORTANT
                    self.setWidgetResizable(True)

                    self.land_polygons = []
                    self.overlay_points = []  # List of QPointF
                    self.selected_hdfs = {}  # Selected hdf files from check boxes

                    # Transform to scale by
                    self.transform = QTransform()
                    self.map_image = QPixmap(3000, 3000)

                    self.map_label = QLabel()
                    self.map_label.setPixmap(self.map_image)

                    self.setWidget(self.map_label)

                def update_image(self, new_points=[]):
                    qp = QPainter()
                    qp.begin(self.map_image)

                    def draw_points(points):
                        if len(points) > 0:
                            for point, color in points:
                                threshold_color = QColor(color.red(), color.green(), color.blue())
                                if color.alpha() > self.min_vapor_threshold:
                                    threshold_color.setAlpha(color.alpha())
                                else:
                                    threshold_color.setAlpha(0)
                                qp.setPen(threshold_color)
                                qp.drawPoint(point * self.transform)

                    if len(new_points) is 0:
                        self.clear_map()

                        for polygon in self.land_polygons:
                            qp.drawPolygon(polygon * self.transform)

                        draw_points(self.overlay_points)

                    else:
                        draw_points(new_points)

                    qp.end()

                    self.map_label.setPixmap(self.map_image)

                def clear_map(self):
                    self.map_image.fill(self.QT_COLOR_WHITE)

                def set_scale(self, scale):
                    self.transform.reset()
                    self.transform.scale(scale, scale)
                    self.update_image()

                def save_map_image(self, location_path):
                    try:
                        self.map_image.save(location_path)
                    except Exception as e:
                        print e.message

                def set_land_shapes(self, land_shapes):
                    for shape in land_shapes:
                        self.land_polygons.append(QPolygonF(map(lambda x: QPointF(x[0], x[1]), shape)))
                    self.update_image()

                def set_overlay_points(self, hdf_parser_dataset, append=True):
                    if append:
                        new_points = hdf_parser_to_QPoints_Color(hdf_parser_dataset)
                        self.overlay_points += new_points
                        self.update_image(new_points=new_points)
                    else:
                        self.overlay_points = hdf_parser_to_QPoints_Color(hdf_parser_dataset)
                        self.update_image()

                def set_min_vapor_threshold(self, threshold):
                    self.min_vapor_threshold = threshold
                    self.update_image()

                def clear_overlay_points(self):
                    self.overlay_points = []
                    self.update_image()

            class HDFButtonListView(QScrollArea):
                def __init__(self, hdf_manager, map_view):
                    super(QWidget, self).__init__()

                    # Allows widgets to resize IMPORTANT
                    self.setWidgetResizable(True)

                    # Layout for widget (frame) to set self to
                    self.layout = QVBoxLayout()
                    self.frame = QFrame()
                    self.frame.setLayout(self.layout)

                    # pointer to map_view to update from buttons
                    self.map_view = map_view

                    # initialize all buttons and corresponding button functions
                    self.update_buttons(hdf_manager)

                    self.setWidget(self.frame)

                def update_buttons(self, hdf_manager):
                    # Remove all button widgets to stop repeats
                    for i in reversed(range(self.layout.count())):
                        self.layout.itemAt(i).widget().setParent(None)

                    for parser in sorted(hdf_manager.hdf_parsers, key=lambda x: x.data_acquisition_datetime):
                        h_layout = QHBoxLayout()

                        button = QPushButton(str(parser.data_acquisition_datetime))
                        button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
                        check_box = QCheckBox()

                        def button_function(hdf_parser):
                            """
                            Function to update map view to the parser selected
                            :return: function to connect to hdf_list_view buttons
                            """
                            def map_function():
                                self.map_view.set_overlay_points(hdf_parser)
                            return map_function

                        def check_box_function(hdf_parser, box):
                            def map_function():
                                if box.isChecked():
                                    self.map_view.selected_hdfs[box] = hdf_parser
                                else:
                                    del self.map_view.selected_hdfs[box]

                            return map_function

                        button.clicked.connect(button_function(parser))
                        check_box.stateChanged.connect(check_box_function(parser, check_box))
                        h_layout.addWidget(button)
                        h_layout.addWidget(check_box)
                        self.layout.addLayout(h_layout)

        class DownloadWindow(QScrollArea):
            def __init__(self, hdf_manager, hdf_button_list_list_view):
                super(QWidget, self).__init__()
                self.setWindowTitle('Download HDF Files...')

                # Allows widgets to resize IMPORTANT
                self.setWidgetResizable(True)
                self.setMinimumSize(400, 500)

                # Layout for widget (frame) to set self to
                self.layout = QVBoxLayout()
                self.frame = QFrame()
                self.frame.setLayout(self.layout)

                # pointer to hdf_button_list_view to update upon downloading hdf files
                self.hdf_button_list_view = hdf_button_list_list_view

                # Initialize each line of csv file to download buttons/labels
                row_num = 0
                for row in hdf_manager.get_downloader_csv_data():
                    def download_hdf_at_current_index_func(index):
                        """
                        Function to connect to download button
                        :return: Function to connect to download button
                        """
                        def button_function():
                            try:
                                hdf_manager.download_at_csv_index(index)
                                self.hdf_button_list_view.update_buttons(hdf_manager)
                            except Exception as e:
                                print e.message

                        return button_function

                    frame = QFrame()
                    download_row_layout = QHBoxLayout()
                    download_label = QLabel("%d. %s (%s)" % (row_num, str(row[0]), str(row[1])))  # row[1] -> HDF download link
                    download_button = QPushButton('Download')
                    download_button.setMaximumSize(150, 30)
                    download_button.clicked.connect(download_hdf_at_current_index_func(row_num))
                    download_row_layout.addWidget(download_label)
                    download_row_layout.addWidget(download_button)
                    frame.setLayout(download_row_layout)
                    self.layout.addWidget(frame)
                    row_num += 1

                self.setWidget(self.frame)


def hdf_parser_to_QPoints_Color(hdf_parser):
    # Combine water_vapor_infrared_dataset, latitude_dataset, and longitude_dataset
    hdf_datasets = numpy.dstack((hdf_parser.water_vapor_infrared_dataset,
                                 hdf_parser.longitude_dataset + 180,
                                 -(hdf_parser.latitude_dataset + 90) + 180))

    def get_point_and_color_list(row):
        """
        Helper function to map a list of [ [ water_vapor, latitude, longitude ] ... ] to
        a tuple of ( QPointF, QColor ):
            - QPointF = point on canvas from  => latitude, longitude
            - QColor = color for QPointF from => water_vapor_value

        :param row: Array of [ [ water_vapor, latitude, longitude ] ... ]
        :return: map of row array to tuple of ( QPointF, QColor )
        """
        return map(lambda x: (QPointF(float(x[1]), float(x[2])),
                              QColor(0, 0, 255, float(max(0, (min(10, x[0]) / 10) * 255)))),
                   row)

    # Combine all rows into one list
    # itertools.chain acts as an iterator for multiple lists
    return list(itertools.chain.from_iterable(map(lambda x: get_point_and_color_list(x), hdf_datasets)))
