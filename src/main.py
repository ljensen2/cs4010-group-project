import os.path
from model.MODISHDFManager import MODISHDFManager
from controller.listeners import HDFDownloadProgressListener
from view.GUI import GUI

from model import geoshape

os.chdir(os.path.dirname(__file__))

listener = HDFDownloadProgressListener()

manager = MODISHDFManager(initial_hdf_files_path='../res/hdf_data',
                          csv_downloads_file_path='../res/csv/LAADS_query.2019-05-21T16_11.csv')

manager.add_download_listener(listener)

usa_shape = geoshape.get_state_shape_points('../res/state_boundaries/usa.dbf',
                                            '../res/state_boundaries/usa.dbf',
                                            absolute_coordinates=True)

with GUI('Atmospheric Rivers', usa_shape, manager) as gui:
    gui.show()

print 'done'
