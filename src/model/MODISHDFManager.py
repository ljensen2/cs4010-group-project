from MODISHDFParser import MODISHDFParser
from MODISHDFDownloader import MODISHDFDownloader
import os


class DownloadException(Exception):
    """
    Custom Exception for handling an attempt to download HDF files
    """
    def __init__(self):
        super(DownloadException, self).__init__('No HDF Downloader... Supply csv downloads path in constructor')


class MODISHDFManager:
    def __init__(self, initial_hdf_files_path=None, csv_downloads_file_path=None):
        self.index = 0
        self.hdf_parsers = []

        # Add existing hdf files to hdf_parsers list - type -> MODISHDFParser
        if initial_hdf_files_path is not None:
            for hdf_file in os.listdir(initial_hdf_files_path):
                self.hdf_parsers.append(MODISHDFParser(os.path.join(initial_hdf_files_path, hdf_file)))

        # If supplied path to csv file, create associating MODISHDFDownloader
        if csv_downloads_file_path is not None:
            self.hdf_downloader = MODISHDFDownloader(csv_downloads_file_path)
        else:
            self.hdf_downloader = None

    def __getitem__(self, index):
        self.index = index
        return self.hdf_parsers[self.index]

    def __iter__(self):
        return self.hdf_parsers.__iter__()

    def __len__(self):
        return len(self.hdf_parsers)

    def __repr__(self):
        rep = "%s: %d items\n" % (str(self.__class__.__name__), len(self))
        for parser in self:
            rep += "\t%s\n" % str(parser)
        return rep

    def get_next(self):
        self.index += 1
        if self.index >= len(self):
            self.index = 0
        return self[self.index]

    def get_previous(self):
        self.index -= 1
        if self.index < 0:
            self.index = len(self) - 1
        return self[self.index]

    def add_download_listener(self, listener):
        if self.hdf_downloader is not None:
            self.hdf_downloader.add_listener(listener)
        else:
            raise DownloadException()

    def download_at_csv_index(self, index):
        """
        Download hdf file at _hdf_downloader and add a new parser to hdf_parsers
        for the downloaded file
        :param index: index of csv file to download
        """
        if self.hdf_downloader is not None:
            self.hdf_parsers.append(MODISHDFParser(self.hdf_downloader.download_at_index(index)))
        else:
            raise DownloadException()

    def get_downloader_csv_data(self, verbose=True):
        """
        Property of hdf_downloader's csv_rows instance variable
        :return: hdf_downloader.csv_data
        """
        if self.hdf_downloader is not None:
            if verbose:
                def name_datetime_list_from_csv_row(csv_row):
                    """
                    Helper function to get datetime and satellite name from csv_row
                    :param csv_row: row in csv file
                    :return: list of [satellite name, granule datetime]
                    """
                    hdf_filename = csv_row[1].split('/')[-1]
                    return list([MODISHDFParser.data_type_name_from_file_name(hdf_filename),
                                 MODISHDFParser.datetime_acquisition_from_file_name(hdf_filename)])

                return map(lambda x: name_datetime_list_from_csv_row(x), self.get_downloader_csv_data(verbose=False))
            else:
                return self.hdf_downloader.csv_data
        else:
            raise DownloadException()

    def download_at_datetime(self, year, month, day, hour, minute):
        """
        TODO: Implement method
        Unimplemented method
        :param year:
        :param month:
        :param day:
        :param hour:
        :param minute:
        :return:
        """
        pass
