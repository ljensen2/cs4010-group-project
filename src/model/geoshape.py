import shapefile

CALIFORNIA_SHAPE_INDEX = 0
NORMALIZE_MULTIPLIER = 100000  # For California shape


def absolute_coordinate_points(coordinate_points):
    """
    Map coordinate values as follows:
        Longitude:(-180 - 180)  => (0 - 360)
        Latitude: (-90  - 90)   => (0 - 180)

    :param coordinate_points: tuple of coordinate values
    :return: tuple of normalized coordinate values
    """
    return map(lambda x: tuple([x[0] + 180, (-(x[1] + 90)) + 180]), coordinate_points)


def get_state_shape_points(shapefile_path, shapefile_database_path, absolute_coordinates=False):
    """
    Returns the points that make up the boundaries of a U.S. state in a shapefile

    Shapefile retrieved from: https://data.ca.gov/dataset/ca-geographic-boundaries

    All geodetic coordinates from file are a magnitude of NORMALIZE_MULTIPLIER larger than actual coordinates

    :param shapefile_path: path to .shp file
    :param shapefile_database_path: path to .dbf file
    :param absolute_coordinates: Add 90 to latitude and 180 to latitude to force lat and lon are greater than 0
           latitude adjusted so it ascends from North to South rather than South to North
    :return: list of list of points that make boundaries of shape (state)
    """
    shapes = []
    with shapefile.Reader(shapefile_path, shapefile_database_path) as shape_reader:
        for shape in shape_reader.iterShapes():
            previous_part_index = 0
            shape.parts.append(len(shape.points) - 1)  # Append index of last shape point

            for part_index in shape.parts:
                points = map(lambda x: tuple([x[0], x[1]]), shape.points[previous_part_index:part_index])
                previous_part_index = part_index

                if absolute_coordinates:
                    points = absolute_coordinate_points(points)

                if len(points) > 0:
                    shapes.append(points)

    return shapes
